# compose-themes: Org HTML themes

[![pipeline status](https://gitlab.inria.fr/compose/include/compose-themes/badges/master/pipeline.svg)](https://gitlab.inria.fr/compose/include/compose--themes/-/commits/master)

Easily export Org documents into HTML pages featuring themes adapted for Inria
in general and for Concace in particular. These themes are based on the Fabrice
Niessen's [readtheorg](https://github.com/fniessen/org-html-themes) theme.

## Themes

### readtheorginria

**readtheorginria** is the most generic theme implements only the corporate
identity of Inria. A light version of the theme using default system fonts
instead of Google(R) fonts is available under the name of
**readtheorginria-light**.

### readtheorgconcace

In addition to the logo of Inria, the **readtheorgconcace** and its light
version **readtheorgconcace-light** feature also the logos of the team partners,
Airbus and CERFACS.

## Usage

To apply a theme, use the corresponding `theme-<theme-name>.setup` as
`#+SETUPFILE` in the target Org document:

```
#+SETUPFILE: https://compose.gitlabpages.inria.fr/include/compose-themes/theme-<theme-name>.setup
```

Note that `<theme-name>` is one of:
- `readtheorginria`
- `readtheorginria-light`
- `readtheorgconcace`
- `readtheorgconcace-light`

In a version-controlled repository using this or the global
[compose-publish](https://gitlab.inria.fr/compose/include/compose-publish)
project as submodule, you would simply specify a local path to the selected
theme file:

```
#+SETUPFILE: ./path/to/the/local/copy/of/theme-<theme-name>.setup
```

## License

So as the [base project](https://github.com/fniessen/org-html-themes) of Fabrice
Niessen, the sources of the hereby themes are available under the terms of the
[GNU General Public License version 3.0](LICENSE). The Inria, Airbus and CERFACS
logos are the property of [Inria](https://www.inria.fr/en),
[Airbus](https://www.airbus.com/) and [CERFACS](https://cerfacs.fr/),
respectively.
